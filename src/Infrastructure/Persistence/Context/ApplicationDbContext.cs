using Finbuckle.MultiTenant;
using FutbolAPI.Application.Common.Events;
using FutbolAPI.Application.Common.Interfaces;
using FutbolAPI.Domain.Catalog;
using FutbolAPI.Domain.Job;
using FutbolAPI.Infrastructure.Persistence.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FutbolAPI.Infrastructure.Persistence.Context;

public class ApplicationDbContext : BaseDbContext
{
    public ApplicationDbContext(
        ITenantInfo currentTenant,
        DbContextOptions options,
        ICurrentUser currentUser,
        ISerializerService serializer,
        IOptions<DatabaseSettings> dbSettings,
        IEventPublisher events
        )
        : base(
            currentTenant,
            options,
            currentUser,
            serializer,
            dbSettings,
            events)
    {
    }
    public DbSet<BackgroundJob> BackgroundJobs => Set<BackgroundJob>();
    public DbSet<Area> Areas => Set<Area>();
    public DbSet<Coach> Coachs => Set<Coach>();
    public DbSet<Competition> Competitions => Set<Competition>();
    public DbSet<Contract> Contracts => Set<Contract>();
    public DbSet<Person> Persons => Set<Person>();
    public DbSet<Player> Players => Set<Player>();
    public DbSet<Season> Seasons => Set<Season>();
    public DbSet<Team> Teams => Set<Team>();
    public DbSet<Winner> Winners => Set<Winner>();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.HasDefaultSchema(SchemaNames.Catalog);
    }
}