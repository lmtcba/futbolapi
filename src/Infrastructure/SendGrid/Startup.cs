﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SendGrid.Extensions.DependencyInjection;
namespace FutbolAPI.Infrastructure.SendGrid;

internal static class Startup
{
    internal static IServiceCollection AddSendGrid(this IServiceCollection services, IConfiguration config)
    {
        var sendGridConfig = config.GetSection(nameof(SendGridSettings)).Get<SendGridSettings>();
        if (!string.IsNullOrEmpty(sendGridConfig.Key))
        {
            services.AddSendGrid(options => options.ApiKey = sendGridConfig.Key);
        }

        return services;
    }
}
