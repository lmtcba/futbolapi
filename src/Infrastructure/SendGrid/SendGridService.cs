﻿using FutbolAPI.Application.Common.SendGrid;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace FutbolAPI.Infrastructure.SendGrid;
public class SendGridService : ISendGridService
{
    private readonly ISendGridClient _sendGridClient;

    public SendGridService(ISendGridClient sendGridClient)
    {
        _sendGridClient = sendGridClient;
    }

    public async Task<string> SendAsync(SendGridRequest request)
    {
        var msg = new SendGridMessage()
        {
            From = new EmailAddress(request.From),
            Subject = request.Subject,
            HtmlContent = request.Body
        };

        var emailAddres = request.To.Select(x => new EmailAddress(x)).ToList();
        msg.AddTos(emailAddres);

        var response = _sendGridClient.SendEmailAsync(msg).Result;
        return response.IsSuccessStatusCode ? "Email Send Successfully" : "Email Sending Failed, Status Code: " + response.StatusCode;
    }
}
