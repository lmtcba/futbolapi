global using FutbolAPI.Application.Common.Models;
global using FutbolAPI.Infrastructure.Auth.Permissions;
global using FutbolAPI.Infrastructure.OpenApi;
global using FutbolAPI.Shared.Authorization;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Mvc;
global using NSwag.Annotations;