﻿using FutbolAPI.Application.Catalog.Competitions;

namespace FutbolAPI.Host.Controllers.Catalog;

public class CompetitionController : VersionedApiController
{

    [HttpPost]
    [MustHavePermission(FSHAction.Create, FSHResource.Competitions)]
    [OpenApiOperation("Create a new Competition in BackroundJob.", "")]
    public Task<Guid> CreateAsync(CreateCompetitionRequest request)
    {
        return Mediator.Send(request);
    }
}