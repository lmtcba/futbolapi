﻿using FutbolAPI.Application.Catalog.Areas;

namespace FutbolAPI.Host.Controllers.Catalog;

public class AreaController : VersionedApiController
{
    [HttpPost("search")]
    [MustHavePermission(FSHAction.Search, FSHResource.Areas)]
    [OpenApiOperation("Search Areas using available filters.", "")]
    public Task<PaginationResponse<AreaDto>> SearchAsync(SearchAreasRequest request)
    {
        return Mediator.Send(request);
    }

    [HttpGet]
    [MustHavePermission(FSHAction.View, FSHResource.Areas)]
    [OpenApiOperation("Get All Areas.", "")]
    public Task<ListResponse<AreaDto>> GetAllAsync()
    {
        return Mediator.Send(new GetAllAreasRequest());
    }

    [HttpGet("{id:guid}")]
    [MustHavePermission(FSHAction.View, FSHResource.Areas)]
    [OpenApiOperation("Get Area details.", "")]
    public Task<AreaDto> GetAsync(Guid id)
    {
        return Mediator.Send(new GetAreaRequest(id));
    }

    [HttpPost]
    [MustHavePermission(FSHAction.Create, FSHResource.Areas)]
    [OpenApiOperation("Create a new Area.", "")]
    public Task<Guid> CreateAsync(CreateAreaRequest request)
    {
        return Mediator.Send(request);
    }

    [HttpPut("{id:guid}")]
    [MustHavePermission(FSHAction.Update, FSHResource.Areas)]
    [OpenApiOperation("Update an Area.", "")]
    public async Task<ActionResult<Guid>> UpdateAsync(UpdateAreaRequest request, Guid id)
    {
        return id != request.Id
            ? BadRequest()
            : Ok(await Mediator.Send(request));
    }

    [HttpDelete("{id:guid}")]
    [MustHavePermission(FSHAction.Delete, FSHResource.Areas)]
    [OpenApiOperation("Delete an Area.", "")]
    public Task<Guid> DeleteAsync(Guid id)
    {
        return Mediator.Send(new DeleteAreaRequest(id));
    }
}