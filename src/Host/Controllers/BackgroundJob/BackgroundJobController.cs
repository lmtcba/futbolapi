﻿using Microsoft.Extensions.FileProviders;
using FutbolAPI.Application.BackgroundJobs;

namespace FutbolAPI.Host.Controllers.BackgroundJob;
public class BackgroundJobController : VersionedApiController
{
    [HttpGet]
    public Task<Guid> ProcessAsync([FromQuery] BackgroundJobDto hangfireDto)
    {
        return Mediator.Send(new BackgroundJobProcess(hangfireDto));
    }

    //[HttpPost]
    //public Task<Guid> UploadFile([FromForm] Guid idRfq, [FromForm] IFormFile file)
    //{
    //    string localFilePath = WriteExcelFile(file).Result;
    //    return Mediator.Send(new BackroundjobImporter(idRfq, localFilePath));
    //}

    private async Task<string> WriteExcelFile(IFormFile file)
    {
        var allowedFiles = new List<string> { ".xls", ".xlsx" };
        string localFilePath = string.Empty;
        if (file.Length > 0)
        {
            string extension = "." + file.FileName.Split(".")[file.FileName.Split(".").Length - 1];
            if (allowedFiles.Any(x => x == extension))
            {
                localFilePath = Path.GetTempFileName();
                using (var stream = new FileStream(localFilePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
        }

        return localFilePath;
    }
}