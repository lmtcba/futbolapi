namespace FutbolAPI.Shared.Multitenancy;

public class MultitenancyConstants
{
    public static class Root
    {
        public const string Id = "root";
        public const string Name = "Root";
        public const string EmailAddress = "lmtcba88@gmail.com";
    }

    public const string DefaultPassword = "Prueba123";

    public const string TenantIdName = "tenant";
}