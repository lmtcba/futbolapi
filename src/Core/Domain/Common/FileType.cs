using System.ComponentModel;

namespace FutbolAPI.Domain.Common;

public enum FileType
{
    [Description(".jpg,.png,.jpeg")]
    Image
}