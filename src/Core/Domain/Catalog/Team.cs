﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Team : AuditableEntity, IAggregateRoot
{
    public Area Area { get; set; }

    [StringLength(50)]
    public string Name { get; set; }

    [StringLength(50)]
    public string ShortName { get; set; }

    [StringLength(100)]
    public string TLA { get; set; }
    public string Crest { get; set; }
    public string Address { get; set; }
    public string Website { get; set; }
    public int Founded { get; set; }
    public string ClubColors { get; set; }
    public string Venue { get; set; }
    public Season CurrentSeason { get; set; }
    public IList<Competition> RunningCompetitions { get; set; }

    public void Update(string name)
    {
        throw new NotImplementedException();
    }
}