﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Competition : AuditableEntity, IAggregateRoot
{
    public Area Area { get; set; }

    [StringLength(50)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(100)]
    public string Type { get; set; }
    public string Emblem { get; set; }
    public Season CurrentSeason { get; set; }
    public IList<Area> Seasons { get; set; }
}
