﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Area : AuditableEntity, IAggregateRoot
{
    [StringLength(100)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(50)]
    public string? Flag { get; set; }

    [StringLength(100)]
    public string? ParentAreaId { get; set; }
    public string? ParentArea { get; set; }
    public IList<Area> ChildAreas { get; set; }

    public void Update(string name)
    {
        throw new NotImplementedException();
    }
}
