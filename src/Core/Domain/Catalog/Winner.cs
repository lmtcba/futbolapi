﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Winner : AuditableEntity, IAggregateRoot
{
    public string Name { get; set; }
    public string ShortName { get; set; }
    public string TLA { get; set; }
    public string CREST { get; set; }
    public string Adress { get; set; }
    public string Website { get; set; }
    public int Founded { get; set; }
    public string ClubColors { get; set; }
    public string Venue { get; set; }
    public DateTime LastUpdate { get; set; }
    public string Stages { get; set; }
}
