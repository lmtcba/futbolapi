﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Person : AuditableEntity, IAggregateRoot
{
    public string FirstName { get; set; }

    [StringLength(50)]
    public string LastName { get; set; }

    [StringLength(50)]
    public string Name { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string Nationality { get; set; }
}
