﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Domain.Catalog;
public class Player : Person
{
    public Team Team { get; set; }
}
