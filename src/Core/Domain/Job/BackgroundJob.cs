﻿namespace FutbolAPI.Domain.Job;
public class BackgroundJob : AuditableEntity, IAggregateRoot
{
    public string? Description { get; private set; }
    public string Status { get; set; }
    public int JobPercentageComplete { get; set; }

    public BackgroundJob(string? description)
    {
        Description = description;
        JobPercentageComplete = 0;
        Status = "Created";
    }

    public BackgroundJob Update(int jobPercentageComplete, string? status)
    {
        JobPercentageComplete = jobPercentageComplete;
        if (status is not null && Status?.Equals(status) is not true) Status = status;
        return this;
    }
}