namespace FutbolAPI.Application.Common.Models;

public static class ListResponseExtensions
{
    public static async Task<ListResponse<TDestination>> AllListAsync<T, TDestination>(
        this IReadRepositoryBase<T> repository, ISpecification<T, TDestination> spec, CancellationToken cancellationToken = default)
        where T : class
        where TDestination : class, IDto
    {
        var list = await repository.ListAsync(spec);
        
        return new ListResponse<TDestination>(list);
    }
}