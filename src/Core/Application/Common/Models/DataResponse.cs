﻿namespace FutbolAPI.Application.Common.Models;

public class DataResponse<T>
{
    public DataResponse(T data)
    {
        Data = data;
    }
    public T Data { get; set; }
}
