﻿namespace FutbolAPI.Application.Common.Models;

public class ListResponse<T>
{
    public ListResponse(List<T> data)
    {
        Data = data;
    }

    public List<T> Data { get; set; }
}
