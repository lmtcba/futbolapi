﻿namespace FutbolAPI.Application.Common.SendGrid;
public class SendGridRequest
{
    public string Subject { get; set; }
    public string Body { get; set; }
    public string From { get; set; }
    public List<string> To { get; set; }
}
