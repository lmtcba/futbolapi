﻿namespace FutbolAPI.Application.Common.SendGrid;

public interface ISendGridService : ITransientService
{
    Task<string> SendAsync(SendGridRequest request);
}