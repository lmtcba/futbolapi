using FutbolAPI.Shared.Events;

namespace FutbolAPI.Application.Common.Events;

public interface IEventPublisher : ITransientService
{
    Task PublishAsync(IEvent @event);
}