﻿using FutbolAPI.Application.Catalog.Competitions;
using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Application.Catalog.Competitions;

public class CreateCompetitionRequest : IRequest<Guid>
{
    public Area Area { get; set; }

    [StringLength(50)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(100)]
    public string Type { get; set; }
    public string Emblem { get; set; }
    public Season CurrentSeason { get; set; }
    public IList<Area> Seasons { get; set; }
}

public class CreateCompetitionRequestValidator : CustomValidator<CreateCompetitionRequest>
{
    public CreateCompetitionRequestValidator(IReadRepository<Competition> repository, IStringLocalizer<CreateCompetitionRequestValidator> localizer) =>
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(75)
            .MustAsync(async (name, ct) => await repository.GetBySpecAsync(new CompetitionByNameSpec(name), ct) is null)
                .WithMessage((_, name) => string.Format(localizer["Competition.alreadyexists"], name));
}

public class CreateCompetitionRequestHandler : IRequestHandler<CreateCompetitionRequest, Guid>
{
    // Add Domain Events automatically by using IRepositoryWithEvents
    private readonly IRepositoryWithEvents<Competition> _repository;

    public CreateCompetitionRequestHandler(IRepositoryWithEvents<Competition> repository) => _repository = repository;

    public async Task<Guid> Handle(CreateCompetitionRequest request, CancellationToken cancellationToken)
    {
        var Competition = new Competition() { Name = request.Name };

        await _repository.AddAsync(Competition, cancellationToken);

        return Competition.Id;
    }
}