﻿namespace FutbolAPI.Application.Catalog.Competitions;

public class CompetitionByNameSpec : Specification<Competition>, ISingleResultSpecification
{
    public CompetitionByNameSpec(string name) =>
        Query.Where(b => b.Name == name);
}