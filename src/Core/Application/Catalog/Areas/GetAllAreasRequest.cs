﻿namespace FutbolAPI.Application.Catalog.Areas;

public class GetAllAreasRequest : IRequest<ListResponse<AreaDto>>
{
}

public class GetAllAreasRequestHandler : IRequestHandler<GetAllAreasRequest, ListResponse<AreaDto>>
{
    private readonly IReadRepository<Area> _repository;

    public GetAllAreasRequestHandler(IReadRepository<Area> repository) => _repository = repository;


    public class AreaGetAllSpec : Specification<Area, AreaDto>
    {
        public AreaGetAllSpec() =>
            Query.Where(p => 1 == 1);
    }

    public async Task<ListResponse<AreaDto>> Handle(GetAllAreasRequest request, CancellationToken cancellationToken)
    {
        return await _repository.AllListAsync(
            (ISpecification<Area, AreaDto>)new AreaGetAllSpec(),
            cancellationToken: cancellationToken);
    }
}