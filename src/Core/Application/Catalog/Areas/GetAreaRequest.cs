﻿namespace FutbolAPI.Application.Catalog.Areas;

public class GetAreaRequest : IRequest<AreaDto>
{
    public Guid Id { get; set; }

    public GetAreaRequest(Guid id) => Id = id;
}

public class AreaByIdSpec : Specification<FutbolAPI.Domain.Catalog.Area, AreaDto>, ISingleResultSpecification
{
    public AreaByIdSpec(Guid id) =>
        Query.Where(p => p.Id == id);
}

public class GetAreaRequestHandler : IRequestHandler<GetAreaRequest, AreaDto>
{
    private readonly IRepository<FutbolAPI.Domain.Catalog.Area> _repository;
    private readonly IStringLocalizer<GetAreaRequestHandler> _localizer;

    public GetAreaRequestHandler(IRepository<FutbolAPI.Domain.Catalog.Area> repository, IStringLocalizer<GetAreaRequestHandler> localizer) => (_repository, _localizer) = (repository, localizer);

    public async Task<AreaDto> Handle(GetAreaRequest request, CancellationToken cancellationToken) =>
        await _repository.GetBySpecAsync(
            (ISpecification<FutbolAPI.Domain.Catalog.Area, AreaDto>)new AreaByIdSpec(request.Id), cancellationToken)
        ?? throw new NotFoundException(string.Format(_localizer["Area.notfound"], request.Id));
}