﻿namespace FutbolAPI.Application.Catalog.Areas;

public class SearchAreasRequest : PaginationFilter, IRequest<PaginationResponse<AreaDto>>
{
}

public class AreasBySearchRequestSpec : EntitiesByPaginationFilterSpec<Area, AreaDto>
{
    public AreasBySearchRequestSpec(SearchAreasRequest request)
        : base(request) =>
        Query.OrderBy(c => c.Name, !request.HasOrderBy());
}

public class SearchAreasRequestHandler : IRequestHandler<SearchAreasRequest, PaginationResponse<AreaDto>>
{
    private readonly IReadRepository<Area> _repository;

    public SearchAreasRequestHandler(IReadRepository<Area> repository) => _repository = repository;

    public async Task<PaginationResponse<AreaDto>> Handle(SearchAreasRequest request, CancellationToken cancellationToken)
    {
        var spec = new AreasBySearchRequestSpec(request);
        return await _repository.PaginatedListAsync(spec, request.PageNumber, request.PageSize, cancellationToken);
    }
}