﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Application.Catalog.Areas;

public class UpdateAreaRequest : IRequest<Guid>
{
    public Guid Id { get; set; }
    [StringLength(100)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(50)]
    public string? Flag { get; set; }

    [StringLength(100)]
    public string? ParentAreaId { get; set; }
    public string? ParentArea { get; set; }
    public IList<Area> ChildAreas { get; set; }
}

public class UpdateAreaRequestValidator : CustomValidator<UpdateAreaRequest>
{
    public UpdateAreaRequestValidator(IRepository<Area> repository, IStringLocalizer<UpdateAreaRequestValidator> localizer) =>
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(75)
            .MustAsync(async (Area, name, ct) =>
                    await repository.GetBySpecAsync(new AreaByNameSpec(name), ct)
                        is not Area existingArea || existingArea.Id == Area.Id)
                .WithMessage((_, name) => string.Format(localizer["Area.alreadyexists"], name));
}

public class UpdateAreaRequestHandler : IRequestHandler<UpdateAreaRequest, Guid>
{
    // Add Domain Events automatically by using IRepositoryWithEvents
    private readonly IRepositoryWithEvents<Area> _repository;
    private readonly IStringLocalizer<UpdateAreaRequestHandler> _localizer;

    public UpdateAreaRequestHandler(IRepositoryWithEvents<Area> repository, IStringLocalizer<UpdateAreaRequestHandler> localizer) =>
        (_repository, _localizer) = (repository, localizer);

    public async Task<Guid> Handle(UpdateAreaRequest request, CancellationToken cancellationToken)
    {
        Area Area = await _repository.GetByIdAsync(request.Id, cancellationToken);

        _ = Area ?? throw new NotFoundException(string.Format(_localizer["Area.notfound"], request.Id));

        Area.Update(request.Name);

        await _repository.UpdateAsync(Area, cancellationToken);

        return request.Id;
    }
}