﻿namespace FutbolAPI.Application.Catalog.Areas;

public class AreaByNameSpec : Specification<Area>, ISingleResultSpecification
{
    public AreaByNameSpec(string name) =>
        Query.Where(b => b.Name == name);
}