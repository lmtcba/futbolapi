﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Application.Catalog.Areas;

public class CreateAreaRequest : IRequest<Guid>
{
    [StringLength(100)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(50)]
    public string? Flag { get; set; }

    [StringLength(100)]
    public string? ParentAreaId { get; set; }
    public string? ParentArea { get; set; }
    public IList<Area> ChildAreas { get; set; }
}

public class CreateAreaRequestValidator : CustomValidator<CreateAreaRequest>
{
    public CreateAreaRequestValidator(IReadRepository<Area> repository, IStringLocalizer<CreateAreaRequestValidator> localizer) =>
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(75)
            .MustAsync(async (name, ct) => await repository.GetBySpecAsync(new AreaByNameSpec(name), ct) is null)
                .WithMessage((_, name) => string.Format(localizer["Area.alreadyexists"], name));
}

public class CreateAreaRequestHandler : IRequestHandler<CreateAreaRequest, Guid>
{
    // Add Domain Events automatically by using IRepositoryWithEvents
    private readonly IRepositoryWithEvents<Area> _repository;

    public CreateAreaRequestHandler(IRepositoryWithEvents<Area> repository) => _repository = repository;

    public async Task<Guid> Handle(CreateAreaRequest request, CancellationToken cancellationToken)
    {
        var Area = new Area() { Name = request.Name, Code = request.Code, ParentArea = request.ParentArea };

        await _repository.AddAsync(Area, cancellationToken);

        return Area.Id;
    }
}