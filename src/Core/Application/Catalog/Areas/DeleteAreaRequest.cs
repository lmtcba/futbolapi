﻿namespace FutbolAPI.Application.Catalog.Areas;

public class DeleteAreaRequest : IRequest<Guid>
{
    public Guid Id { get; set; }

    public DeleteAreaRequest(Guid id) => Id = id;
}

public class DeleteAreaRequestHandler : IRequestHandler<DeleteAreaRequest, Guid>
{
    private readonly IRepositoryWithEvents<Area> _AreaRepo;
    private readonly IStringLocalizer<DeleteAreaRequestHandler> _localizer;

    public DeleteAreaRequestHandler(IRepositoryWithEvents<Area> AreaRepo, IStringLocalizer<DeleteAreaRequestHandler> localizer) =>
        (_AreaRepo, _localizer) = (AreaRepo, localizer);

    public async Task<Guid> Handle(DeleteAreaRequest request, CancellationToken cancellationToken)
    {
        var Area = await _AreaRepo.GetByIdAsync(request.Id, cancellationToken);

        _ = Area ?? throw new NotFoundException(_localizer["Area.notfound"]);

        await _AreaRepo.DeleteAsync(Area, cancellationToken);

        return request.Id;
    }
}