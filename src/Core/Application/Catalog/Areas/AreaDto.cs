﻿using System.ComponentModel.DataAnnotations;

namespace FutbolAPI.Application.Catalog.Areas;

public class AreaDto : IDto
{
    public Guid Id { get; set; }
    [StringLength(100)]
    public string Name { get; set; }

    [StringLength(50)]
    public string Code { get; set; }

    [StringLength(50)]
    public string? Flag { get; set; }

    [StringLength(100)]
    public string? ParentAreaId { get; set; }
    public string? ParentArea { get; set; }
    public IList<Area> ChildAreas { get; set; }
}