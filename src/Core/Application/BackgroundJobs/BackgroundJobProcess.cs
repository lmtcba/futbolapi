﻿using FutbolAPI.Domain.Job;

namespace FutbolAPI.Application.BackgroundJobs;
public class BackgroundJobProcess : IRequest<Guid>
{
    public BackgroundJobDto job { get; set; }

    public BackgroundJobProcess(BackgroundJobDto job) => this.job = job;
}

public class HangfireProcessRequestHandler : IRequestHandler<BackgroundJobProcess, Guid>
{
    private readonly IRepositoryWithEvents<BackgroundJob> _repository;

    private readonly IJobService _hangfireService;

    public HangfireProcessRequestHandler(IJobService hangfireService, IRepositoryWithEvents<BackgroundJob> repository)
    {
        _hangfireService = hangfireService;
        _repository = repository;
    }

    public void SimulateTask(int millisecondsTimeOut, string taskMessage, Guid backgroundJobId, CancellationToken cancellationToken)
    {
        var backgroundJob = _repository.GetByIdAsync(backgroundJobId, cancellationToken).Result;
        int internalMillisecondsTimeOut = millisecondsTimeOut / 10;

        for (int i = 1; i <= 10; i++)
        {
            backgroundJob?.Update(i * 10, "Run steep: " + i);
            _repository.UpdateAsync(backgroundJob, cancellationToken);

            Thread.Sleep(internalMillisecondsTimeOut);
        }

        backgroundJob?.Update(100, "Done");
        _repository.UpdateAsync(backgroundJob, cancellationToken);
    }

    public async Task<Guid> Handle(BackgroundJobProcess request, CancellationToken cancellationToken)
    {
        var backgroundJob = new BackgroundJob(request.job.Description);
        var job = await _repository.AddAsync(backgroundJob, cancellationToken);
        var hangfireId = _hangfireService.Enqueue(() => SimulateTask(request.job.MillisecondTimeOut, request.job.Description, job.Id, cancellationToken));
        return job.Id;
    }
}