﻿namespace FutbolAPI.Application.BackgroundJobs;
public class BackgroundJobDto
{
    public int MillisecondTimeOut { get; set; }
    public string? Description { get; set; }
}